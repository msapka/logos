#!/bin/bash
#__  __   ___ 
#|  \/  | / __|   Michal Sapka (D-S)
#| |\/| | \__ \   https://michal.sapka.pl
#|_|  |_| |___/   gemini://michal.sapka.pl
#

IFS=''

sufix=(	
	'2s/.*/&   Michal Sapka (D-S)/'
	'3s/.*/&   https:\/\/michal.sapka.pl/'
#	'4s/.*/&   gemini:\/\/michal.sapka.pl/'
)

echo "Clearing old files..."
rm ./logos/*
rm ./comments/*
rm ./img/*

input_files=(small medium big ansi)

echo "Preparing logo files..."

for file in ${input_files[@]}; do
	filename="raw/${file}.txt"
	echo "Reading from ${filename}..."

	 awk -v prefix="$file" -v RS= '{print > ("logos/" prefix "-" NR ".txt")}' $filename
	 awk -v prefix="$file" -v RS= '{print > ("comments/" prefix "-" NR ".txt")}' $filename
done

echo "Preparing comment files..."

for file in comments/*.txt; do
	echo "Adding line sufex to $(basename $file)..." 

	for comment in ${sufix[@]}; do
		sed -i'' -e $comment $file
	done
done

find ./comments -name '*e' -delete


echo "Preparing graphical logos..."
for file in logos/*.txt; do
	echo "Adding image to $(basename $file)..." 
	cat $file | convert -size 1000x2000 xc:white -font "Monaco" \
		-pointsize 24 -fill black -annotate +1+1 "@-" -trim -bordercolor "#FFF" \
		-border 10 +repage "img/$(basename $file ".txt").png"
done

